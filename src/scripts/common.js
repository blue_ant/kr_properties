/*=require ./includes/chunks/APP_CONFIGS.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/blocks/*.js */

const currentDevice = device.default;

$(".DropdownMenu").menu({
    icons: {
        submenu: "hidden",
    },

    position: {
        my: "center bottom",
        at: "center top-10",
        // of: ".Header_inner",
    },
    classes: {
        //"ui-menu-icon": "hidden",
    },
});

// START: Mobile menu
$(".Header_burger").fancybox({
    src: "#mobileMenuModal",
    type: "inline",
    infobar: false,
    arrows: false,
    keyboard: false,
    smallBtn: false,
    touch: false,
    baseTpl:
        '<div class="fancybox-container">' +
        '<div class="Modal_backdrop fancybox-bg"></div>' +
        '<div class="fancybox-stage"></div>' +
        "</div>",
});

$(".MobMenu_link-withSubmenu").on("click", (event) => {
    event.preventDefault();
    $(event.currentTarget)
        .next()
        .toggleClass("MobMenu_sublist-visible");
});
// END: Mobile menu

$(".Footer_callOrderLink, .Header_callOrderLink").fancybox({
    touch: false,
    arrows: false,
    buttons: [],
    autoFocus: true,
});

new InteractiveForm($("#orderModal").find("form"), {
    submitHandler: (form) => {
        let $form = $(form);

        window.pagePreloader.show();

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url: form.action,
            type: form.method,
            data: dataToSend,
        })
            .done((response) => {
                let errorCode = parseInt(response.code);
                if (errorCode === 0) {
                    let successText =
                        `<div class="PopForm Form_success">` +
                        `<div class="PopForm_head">Спасибо!</div>` +
                        `<div class="PopForm_successText">${response.success}</div>` +
                        `</div>`;
                    window.requestAnimationFrame(() => {
                        $form
                            .parent()
                            .hide()
                            .after(successText);
                    });
                } else {
                    alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
                }
            })
            .always((/*response*/) => {
                window.pagePreloader.hide();
            });
    },
});

$(document).ready(function() {
    window.pagePreloader = new Preloader("#pagePreloader");
    window.pagePreloader.hide();
});
