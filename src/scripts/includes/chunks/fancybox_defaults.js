$.extend($.fancybox.defaults, {
    hash: false,
    infobar: false,
    buttons: ["close"],
    margin: [40, 40],
    lang: "ru",
    backFocus: false,
    touch: {
        vertical: false,
        momentum: true,
    },
    mobile: {
        clickContent: false,
    },

    i18n: {
        ru: {
            CLOSE: "Закрыть",
            NEXT: "Далее",
            PREV: "Назад",
            ERROR: "Не удалось открыть галерею - попробуйте позже",
            PLAY_START: "Запустить слайдшоу",
            PLAY_STOP: "Пауза",
            FULL_SCREEN: "На весь экран",
            THUMBS: "Миниатуры",
        },
    },

    afterClose: function() {
        if (this.type === "inline") {
            let $form = this.$content.find("form");
            let validator = $form.trigger("reset").data("validator");
            if (validator) {
                validator.resetForm();
                $form.find("input.error").removeClass("error");
            }

            let $successText = this.$content.find(".Form_success");
            if ($successText.length) {
                $form.parent().show();
                $successText.remove();
            }
        }
    },
});
