class ObjDetails {
    constructor(data) {
        let $elem = $(`<div>${this.render(data)}</div>`);

        if (data.images && data.images.length > 1) {
            this.swiper = new Swiper($elem.find(".swiper-container"), {
                navigation: {
                    prevEl: $elem.find(".ObjDetails_arrow-prev"),
                    nextEl: $elem.find(".ObjDetails_arrow-next"),
                    disabledClass: "ObjDetails_arrow-disabled",
                },
                resistanceRatio: 0,
            });
        }
        $.fancybox.open($elem, {
            touch: false,
            afterShow: () => {
                if (this.swiper) {
                    this.swiper.update();
                }
            },
            afterClose: () => {
                if (this.swiper) {
                    this.swiper.destroy();
                }
            },
        });
    }
    render(data) {
        /*=require ../../ajax/obj_popup.js */
        return template(data);
    }
}
