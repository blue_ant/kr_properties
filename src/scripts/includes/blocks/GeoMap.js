class GeoMap {
    constructor(el, opts) {
        if (!el) {
            console.error('GeoMap class constructor requires "el" argument!');
            return;
        }

        let $el = $(el);

        if (!$el.length) {
            console.error('Map constructor can\'t find given "el" in DOM!');
            return;
        }

        let mapOpts = {
            div: $el.find(".GeoMap_map").get(0),
            lat: APP_CONFIGS.buildingLatLng.lat/* + 0.00801968*/,
            lng: APP_CONFIGS.buildingLatLng.lng/*- 0.01169263*/,
            streetViewControl: false,
            fullscreenControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM,
            },
            zoom: 14,
            styles: APP_CONFIGS.gmapsStyles,
        };

        if (opts) {
            for (let prop in opts) {
                mapOpts[prop] = opts[prop];
            }
        }

        let mapInst = new GMaps(mapOpts);

        mapInst.addMarker({
            position: APP_CONFIGS.buildingLatLng,
            icon: {
                url: "/img/map_pin.png",
                scaledSize: new google.maps.Size(37, 52),
            },
            /*infoWindow: {
                content: APP_CONFIGS.buildingName,
                pixelOffset: new google.maps.Size(-5, 0),
            },*/
            /*details: {
                type: mrk.type
            }*/
        });

        this.$el = $el;
        this.gmapsInst = mapInst;
    }

    //TODO: add destroy method
    /*destroy(){

    }*/
}
