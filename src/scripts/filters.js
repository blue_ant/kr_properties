const render = function() {
    /*=require ./ajax/filters_cards.js */
    return template;
};
const ajax_card = render();
$.fn.select2.amd.define("select2/i18n/ru", [], function() {
    // Russian
    return {
        errorLoading: function() {
            return "Результат не может быть загружен.";
        },
        inputTooLong: function(args) {
            let overChars = args.input.length - args.maximum;
            let message = "Пожалуйста, удалите " + overChars + " символ";
            if (overChars >= 2 && overChars <= 4) {
                message += "а";
            } else if (overChars >= 5) {
                message += "ов";
            }
            return message;
        },
        inputTooShort: function(args) {
            let remainingChars = args.minimum - args.input.length;

            let message = "Пожалуйста, введите " + remainingChars + " или более символов";

            return message;
        },
        loadingMore: function() {
            return "Загружаем ещё ресурсы…";
        },
        maximumSelected: function(args) {
            let message = "Вы можете выбрать " + args.maximum + " элемент";

            if (args.maximum >= 2 && args.maximum <= 4) {
                message += "а";
            } else if (args.maximum >= 5) {
                message += "ов";
            }

            return message;
        },
        noResults: function() {
            return "Ничего не найдено";
        },
        searching: function() {
            return "Поиск…";
        },
    };
});

$(document).ready(function() {
    let filterItemsWrp = $(".Filters_itemsWrp");
    let filtersForm = $(".Filters_form");
    let singleSelect = filtersForm.find(".Filters_select-single select");
    let rangeSelect = filtersForm.find(".Filters_selectRange .Filters_range");
    let multiSelect = filtersForm.find(".Filters_select-multi select");
    let showFilter = $(".Filters_showAll_btn");
    let resetBtn = $(".Filters_resetBtn");

    function declOfNum(number, titles) {
        let num = Math.abs(number) % 100;
        let n1 = num % 10;

        if (num > 10 && num < 20) {
            return titles[2];
        }
        if (n1 > 1 && n1 < 5) {
            return titles[1];
        }
        if (n1 === 1) {
            return titles[0];
        }
        return titles[2];
    }

    function sendForm(isFirstLoad = false, applyFilter = false) {
        if (applyFilter) {
            document.forms.filter.curPage.value = 1;
        }
        const form = $(document.forms.filter);
        const action = form.attr("action");
        const method = form.attr("method");
        const formData = form.serialize();
        if (!isFirstLoad) {
            history.replaceState("", "", `${location.pathname}?${formData}`);
        }
        $.ajax({
            url: action,
            method,
            data: formData,
        }).done((result) => {
            result.lots.forEach((item) => {
                let square = "";
                if (item.squareFrom) {
                    square += `от ${item.squareFrom} м<sup>2</sup>`;
                }
                if (item.squareTo) {
                    square += ` до ${item.squareTo} м<sup>2</sup>`;
                }
                item.square = square;
                item.amountText = `${declOfNum(item.amount, ["вариант", "варианта", "вариантов"])}`;
            });
            let lotsCount = result.count;
            filtersForm
                .find(".Filters_found span")
                .html(`${lotsCount} ${declOfNum(lotsCount, ["вариант", "варианта", "вариантов"])}`);
            filtersForm.find(".Filters_found").css("display", "block");
            if (isFirstLoad) {
                window.pages = Math.ceil(result.count / result.lots.length);
            }
            filterItemsWrp.find(".js-filters-result").html(
                ajax_card({
                    lots: result.lots,
                    pages: window.pages,
                    currentPage: document.forms.filter.curPage.value,
                })
            );
        });
    }

    filterItemsWrp.find(".js-filters-result").on("click", ".Filters_bullet[data-page]", function() {
        document.forms.filter.curPage.value = $(this).data("page");
        sendForm();
    });

    (function getParams() {
        if (filtersForm.data("type") !== "no-ajax") {
            let params = {};
            if (location.search) {
                let parts = location.search.substring(1).split("&");
                for (let i = 0; i < parts.length; i++) {
                    let nv = parts[i].split("=");
                    nv[0] = decodeURIComponent(nv[0]);
                    if (!nv[0]) continue;
                    if (nv[0].indexOf("[]") !== -1) {
                        if (params[nv[0]] === undefined) {
                            params[nv[0]] = [];
                        }
                        params[nv[0]].push(nv[1]);
                    } else {
                        params[nv[0]] = nv[1] || true;
                    }
                }
            }
            if (params) {
                $.each(params, (key, value) => {
                    $(document.forms.filter[key]).val(value);
                });
                sendForm(true);
            }
        }
    })();

    (function createSelects() {
        singleSelect.each(function() {
            $(this).select2({
                minimumResultsForSearch: -1,
                dropdownParent: $(this).parent(),
                placeholder: $(this).attr("data-placeholder"),
                width: "100%",
            });
        });
        multiSelect.each(function() {
            $(this).select2({
                minimumResultsForSearch: -1,
                dropdownParent: $(this).parent(),
                allowClear: true,
                placeholder: $(this).attr("data-placeholder"),
                width: "100%",
            });
        });

        rangeSelect.each(function() {
            let step = parseFloat($(this).attr("data-step"));
            let min = parseFloat(
                $(this)
                    .find("input[data-min]")
                    .attr("data-min")
            );
            let max = parseFloat(
                $(this)
                    .find("input[data-max]")
                    .attr("data-max")
            );
            let from = parseFloat(
                $(this)
                    .find("input[data-min]")
                    .val()
            );
            let to = parseFloat(
                $(this)
                    .find("input[data-max]")
                    .val()
            );
            $(this)
                .find(".Filters_rangeMin")
                .html(from);
            $(this)
                .find(".Filters_rangeMax")
                .html(to);
            $(this).slider({
                language: "ru",
                step,
                range: true,
                min,
                max,
                values: [from, to],
                change: (event, ui) => {
                    $(this)
                        .find("input[data-min]")
                        .val(ui.values[0]);
                    $(this)
                        .find("input[data-max]")
                        .val(ui.values[1]);
                    $(this)
                        .find(".Filters_rangeMin")
                        .html(ui.values[0]);
                    $(this)
                        .find(".Filters_rangeMax")
                        .html(ui.values[1]);
                },
                slide: (event, ui) => {
                    $(this)
                        .find(".Filters_rangeMin")
                        .html(ui.values[0]);
                    $(this)
                        .find(".Filters_rangeMax")
                        .html(ui.values[1]);
                },
            });
        });
    })();

    (function filterShow() {
        showFilter.on("click", function(e) {
            const showFilterBtn = $(this);
            e.preventDefault();
            if (showFilterBtn.hasClass("Filters_showAll_btn--active")) {
                showFilterBtn.removeClass("Filters_showAll_btn--active");
                filtersForm.closest(".Filters_box").slideUp(300);
                showFilterBtn.find("span").text("Развернуть фильтр");
                if (filterItemsWrp) {
                    filterItemsWrp.removeClass("Filters_itemsWrp--darken");
                }
            } else {
                showFilterBtn.addClass("Filters_showAll_btn--active");
                filtersForm.closest(".Filters_box").slideDown();
                filtersForm.find(".select2-search__field").css("width", "100%");
                showFilterBtn.find("span").text("Свернуть фильтр");
                if (filterItemsWrp) {
                    filterItemsWrp.addClass("Filters_itemsWrp--darken");
                }
            }
        });
        $(window).resize(function() {
            if (window.innerWidth >= 768) {
                filtersForm.closest(".Filters_box").css("display", "block");
            } else {
                filtersForm.closest(".Filters_box").css("display", "none");
            }
        });
    })();

    (function sendingDataForm() {
        if (filtersForm.data("type") !== "no-ajax") {
            filtersForm.on("submit", function(e) {
                e.preventDefault();
                sendForm(false, true);
            });

            resetBtn.on("click", function() {
                filtersForm.get(0).reset();
                singleSelect.val(null).trigger("change");
                multiSelect.val(null).trigger("change");
                rangeSelect.each(function() {
                    let $rangeSld = $(this);
                    let min = $rangeSld.slider("option", "min");
                    let max = $rangeSld.slider("option", "max");
                    $rangeSld.slider("option", "values", [min, max]);
                });
                sendForm(false, true);
            });
        }
    })();

    $(".js-filters-result")
        .on("click", ".Rent_optionsMoreBtn", (event) => {
            event.preventDefault();
            pagePreloader.show()
            let $el = $(event.currentTarget);
            let $optionsHidden = $el.parent().prev();
            if ($el.hasClass("Rent_optionsMoreBtn-open")) {
                $el.removeClass("Rent_optionsMoreBtn-open");
                $optionsHidden.slideUp(300);
            } else {
                $el.addClass("Rent_optionsMoreBtn-open");
                $optionsHidden.slideDown(600);
            }
        })
        .on("click", ".Rent_callbackBtn", (event) => {
            event.preventDefault();
            $(".Header_callOrderLink").trigger("click");
        })
        .on("click", ".Rent_optionsRow", (event) => {
            event.preventDefault();
            $.ajax({
                url: event.currentTarget.href,
                dataType: "json",
            })
                .done((data) => {
                    new ObjDetails(data);
                })
                .fail(() => {
                    alert("Не удалось получить данные об объекте! Попробуйте позже.");
                })
                .always(() => {
                    pagePreloader.hide();
                });
        });
});
