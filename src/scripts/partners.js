function updatePartnersGroupItems() {
    $(".Partners_group").each(function() {
        let thisBl = $(this);
        let visibleBlocks = thisBl.find(".Partners_group_item:visible").length;
        let allBlocks = thisBl.find(".Partners_group_item").length;
        thisBl.find(".Partners_group_count_show").text(visibleBlocks);
        thisBl.find(".Partners_group_count_all").text(allBlocks);
    });
}

$(".Partners_group").each(function() {
    let thisBl = $(this);
    let notThisBl = $(".Partners_group").not(this);
    thisBl.find(".Partners_group_item_showAll").click(function() {
        thisBl.toggleClass("Partners_group_active");
        notThisBl.removeClass("Partners_group_active");
        $("html, body").animate({ scrollTop: thisBl.offset().top }, 100);
        updatePartnersGroupItems();
    });
});

updatePartnersGroupItems();

$(window).on("resize", function() {
    updatePartnersGroupItems();
});
