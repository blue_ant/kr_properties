$(document).ready(function() {
  function declOfNum(number, titles) {
    let num = Math.abs(number) % 100;
    let n1 = num % 10;
    if (num > 10 && num < 20) {
      return titles[2];
    }
    if (n1 > 1 && n1 < 5) {
      return titles[1];
    }
    if (n1 === 1) {
      return titles[0];
    }
    return titles[2];
  }

  const rentWrp = $('.Sale_rentWrp');
  const cards = $('.Sale_wrp .Card');
  cards.each(function() {
    const amount = $(this).find('.Card_amount span').text();
    const amountText = $(this).find('.Card_amount div');
    amountText.html(`${declOfNum(amount, ["вариант", "варианта", "вариантов"])}`);
  });
  if ($('.Sale_offerItemImagesWrp').length === 1) {
    const images = $('.Sale_offerItemImagesWrp .Sale_offerImage');
    const btns = $('.Sale_offerItemImagesWrp .Sale_offerBtn');
    btns.each((key, btn) => {
      $(btn).on('click', () => {
        $(images).removeClass('Sale_offerImage-active');
        $(btns).removeClass('Sale_offerBtn-active');
        $(btn).addClass('Sale_offerBtn-active');
        $(images[key]).addClass('Sale_offerImage-active');
      });
    });

  }
  rentWrp.each(function() {
    const thisEl = $(this);
    const slider = thisEl.find('.Sale_rentSlider .swiper-container');

    let RentSlider = new Swiper(slider, {
      slidesPerView: 3,
      slidesPerGroup: 3,
      speed: 600,
      spaceBetween: 20,
      navigation: {
        nextEl: thisEl.find('.Sale_rentSliderBtn-next'),
        prevEl: thisEl.find('.Sale_rentSliderBtn-prev'),
      },
      on: {
        init() {
          thisEl.find('.Sale_slideTotal span').text(this.slides.length);
          thisEl.find('.Sale_slidesAmount-mobile .Sale_slideTotal span').text(this.slides.length);
          if (window.innerWidth < 768) {
            thisEl.find('.Sale_slidesAmount-mobile .Sale_slideCurrent').text('01');
          } else if (window.innerWidth < 1284) {
            thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 2));
          } else {
            thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 3));
          }
        },

        slideChange() {
          if (window.innerWidth < 768) {
            if ((this.activeIndex + 1) <= 9) {
              thisEl.find('.Sale_slidesAmount-mobile .Sale_slideCurrent').text('0' + (this.activeIndex + 1));
            } else {
              thisEl.find('.Sale_slidesAmount-mobile .Sale_slideCurrent').text(this.activeIndex + 1);
            }
          } else if (window.innerWidth < 1284) {
            if ((this.activeIndex + 2) <= 9) {
              thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 2));
            } else {
              thisEl.find('.Sale_slideCurrent').text(this.activeIndex + 2);
            }
          } else {
            if ((this.activeIndex + 3) <= 9) {
              thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 3));
            } else {
              thisEl.find('.Sale_slideCurrent').text(this.activeIndex + 3);
            }
          }
        },
        resize() {
          if (window.innerWidth < 768) {
            thisEl.find('.Sale_slidesAmount-mobile .Sale_slideCurrent').text('01');
          } else if (window.innerWidth < 1284) {
            thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 2));
          } else {
            thisEl.find('.Sale_slideCurrent').text('0' + (this.activeIndex + 3));
          }
        }
      },
      breakpoints: {
        1284: {
          slidesPerView: 2,
          slidesPerGroup: 2
        },
        767: {
          slidesPerView: 1,
          slidesPerGroup: 1
        }
      }
    });

    $(window).on('resize', function () {
      RentSlider.update();
    });
  });

  if ($('.Sale_separateMore').length === 1) {
    const separateCurrent = $('.Sale_separate .Sale_slideCurrent');
    const separateTotal = $('.Sale_separate .Sale_slideTotal span');
    const separateItemsCount = $('.Sale_separateItems .Card').length;
    separateTotal.html(separateItemsCount);
    $('.Sale_separateMore').each((key, item) => {
      let parent = $(item).prev();
      parent.realHeight = parent.height();
      parent.smallHeight = parent.find('.Card:first').height() + 20;
      parent.css('height', parent.smallHeight);
      $(window).on('resize', () => {
        if ($(item).hasClass('Sale_separateMore-open')) {
          parent.realHeight = $('.Sale_separateItems').outerHeight();
          parent.css('height', parent.realHeight);
        } else {
          parent.smallHeight = parent.find('.Card:first').height() + 20;
          parent.css('height', parent.smallHeight);
        }
      });
      $(item).on('click', function() {
        let state = $(item).hasClass('Sale_separateMore-open');
        $(item).toggleClass('Sale_separateMore-open');
        if (state) {
          parent.smallHeight = parent.find('.Card:first').height() + 20;
          parent.animate({'height': parent.smallHeight});
          separateCurrent.html('03');
        } else {
          parent.realHeight = $('.Sale_separateItems').outerHeight();
          parent.animate({'height': parent.realHeight});
          separateCurrent.html(separateItemsCount);
        }
      });
    });
  }
});