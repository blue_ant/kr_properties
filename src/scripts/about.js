(() => {
    let $slider = $(".AboutBlock_slider");

    new Swiper($slider, {
        effect: "slide",
        slidesPerView: 1,
        speed: 600,
        loop: true,
        navigation: {
            nextEl: $slider.find(".AboutBlock_sliderArrow-next"),
            prevEl: $slider.find(".AboutBlock_sliderArrow-prev"),
        },
    });

    if (currentDevice.desktop()) {
        $(".AboutBlock_itemText-scroll").each(function() {
            new PerfectScrollbar(this, {
                maxScrollbarLength: 52,
            });
        });
    }

    $(".About_planMarker").hover(
        function() {
            let curId = $(this).attr("data-id");
            $(".About_planList li[data-id='" + curId + "']").addClass("hover");
        },
        function() {
            $(".About_planList li").removeClass("hover");
        }
    );

    let $itemsSlider = $(".About_itemSlider");

    new Swiper($itemsSlider.find(".swiper-container"), {
        slidesPerView: 3,
        slidesPerGroup: 3,
        speed: 600,
        spaceBetween: 20,
        navigation: {
            nextEl: $itemsSlider.find(".About_itemSliderNext"),
            prevEl: $itemsSlider.find(".About_itemSliderPrev"),
        },
        breakpoints: {
            1283: {
                slidesPerView: 2,
                slidesPerGroup: 2,
            },
            767: {
                slidesPerView: 1,
                slidesPerGroup: 1,
            },
        },
        on: {
            init() {
                $itemsSlider.find(".About_sliderCountTotal span").text(this.slides.length);
                if (window.innerWidth < 768) {
                    $itemsSlider.find(".About_sliderCountCurrent").text(1);
                } else if (window.innerWidth < 1284) {
                    $itemsSlider.find(".About_sliderCountCurrent").text(this.activeIndex + 1);
                } else {
                    $itemsSlider.find(".About_sliderCountCurrent").text(this.activeIndex + 3);
                }
            },

            slideChange() {
                if (window.innerWidth < 768) {
                    $itemsSlider.find(".About_sliderCountCurrent").text(this.activeIndex + 1);
                } else if (window.innerWidth < 1284) {
                    $itemsSlider.find(".About_sliderCountCurrent").text(this.activeIndex + 2);
                } else {
                    $itemsSlider.find(".About_sliderCountCurrent").text(this.activeIndex + 3);
                }
            },
        },
    });

    let $mainSliderWrp = $(".About_mainSlider");
    let $thumbSliderWrp = $(".About_thumbSlider");

    let mainSlider = new Swiper($mainSliderWrp.find(".swiper-container"), {
        slidesPerView: 1,
        speed: 600,
        freeMode: false,
        navigation: {
            nextEl: $mainSliderWrp.find(".About_mainSliderNext"),
            prevEl: $mainSliderWrp.find(".About_mainSliderPrev"),
        },
        on: {
            slideChange: function() {
                let activeIndex = this.activeIndex;
                thumbSlider.slides.removeClass("active");
                thumbSlider.slides.eq(activeIndex).addClass("active");
                thumbSlider.slideTo(activeIndex, 800, false);
            },
        },
    });

    let thumbSlider = new Swiper($thumbSliderWrp, {
        slidesPerView: "auto",
        freeMode: true,
        spaceBetween: 20,
        speed: 600,
        slideToClickedSlide: true,
        on: {
            init: function() {
                $(this.slides[0]).addClass("active");
            },
            click: function() {
                let clicked = this.clickedIndex;
                thumbSlider.slides.removeClass("active");
                $(this.clickedSlide).addClass("active");
                mainSlider.slideTo(clicked, 800, false);
            },
        },
        breakpoints: {
            1283: {
                spaceBetween: 14,
            },
            767: {
                spaceBetween: 8,
            },
        },
    });

    $(".About_moreWrp").each(function() {
        let more = $(this);
        let moreBtn = more.find(".About_moreBtn");
        let btnText = more.find(".About_moreBtn span");
        moreBtn.on("click", function() {
            if ($(this).hasClass("About_moreBtn-open")) {
                $(this).removeClass("About_moreBtn-open");
                btnText.text("Развернуть");
            } else {
                $(this).addClass("About_moreBtn-open");
                btnText.text("Свернуть");
            }
        });
    });

    let $table = $(".About_tableWrp");
    let rowCount = $table.find(".About_tableRow").length;
    let $tableSlideWrp = $table.find(".About_tableSlideWrp");

    $table.find(".About_moreBtn div div").text(rowCount);

    $table.find(".About_moreBtn").on("click", function() {
        if ($(this).hasClass("About_moreBtn-open")) {
            $table
                .find(".About_tableRow:hidden")
                .addClass("open")
                .css("display", "table-row");
            let tableHeight = $table.find(".About_table").innerHeight();
            $tableSlideWrp.css("max-height", tableHeight + "px");
        } else {
            $table
                .find(".About_tableRow.open")
                .removeClass("open")
                .css("display", "none");
            $tableSlideWrp.css("max-height", "");
        }
    });

    $(".About_showMore").each(function() {
        let $el = $(this);
        let showMoreItem = $el.find(".About_showMoreItem");
        let visibleCount = $el.find(".About_showMoreItem:visible").length;

        $el.find(".About_sliderCountCurrent").text(visibleCount);
        $el.find(".About_sliderCountTotal span").text(showMoreItem.length);
        $el.find(".About_moreBtn").on("click", function() {
            if ($(this).hasClass("About_moreBtn-open")) {
                $el.find(".About_sliderCountCurrent").text(showMoreItem.length);
                $el
                    .find(".About_showMoreItem:hidden")
                    .slideDown(600)
                    .addClass("open");
            } else {
                $el
                    .find(".About_showMoreItem.open")
                    .slideUp(300)
                    .removeClass("open");
                $el.find(".About_sliderCountCurrent").text(visibleCount);
            }
        });
    });

    let aboutMap = new GMaps({
        div: ".About_map",
        lat: APP_CONFIGS.buildingLatLng.lat,
        lng: APP_CONFIGS.buildingLatLng.lng,
        disableDefaultUI: true,
        zoomControl: true,
        zoom: 13,
        styles: APP_CONFIGS.gmapsStyles,
    });

    aboutMap.addMarker({
        lat: APP_CONFIGS.buildingLatLng.lat,
        lng: APP_CONFIGS.buildingLatLng.lng,
        title: "Красная роза",
        icon: {
            url: "/img/map_pin.png",
            scaledSize: new google.maps.Size(37, 52),
        },
    });
})();
