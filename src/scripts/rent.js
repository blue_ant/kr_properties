let options = $('.Rent_itemOptions');
options.each(function () {
    let option = $(this);
    let moreBtn = option.find('.Rent_optionsMoreBtn');
    let optionsHidden = option.find('.Rent_optionsHidden');
    moreBtn.on('click', function () {
        if ($(this).hasClass('Rent_optionsMoreBtn-open')) {
            $(this).removeClass('Rent_optionsMoreBtn-open');
            optionsHidden.slideUp(300);
        } else {
            $(this).addClass('Rent_optionsMoreBtn-open');
            optionsHidden.slideDown(600);
        }
    })
});

let RentSliderWrp = $('.Rent_slider .swiper-container');

let RentSlider = new Swiper(RentSliderWrp, {
    slidesPerView: 1,
    speed: 600,
    navigation: {
        nextEl: '.Rent_sliderBtn-next',
        prevEl: '.Rent_sliderBtn-prev',
    },
    on: {
        init() {
            $(this.$el).find('.Rent_slideTotal span').text(this.slides.length);
            $(this.$el).find('.Rent_slideCurrent').text(1);
            $('.Rent_slidesAmount-mobile .Rent_slideTotal span').text(this.slides.length);
            $('.Rent_slidesAmount-mobile .Rent_slideCurrent').text(1);
        },

        slideChange() {
            $(this.$el).find('.Rent_slideCurrent').text(this.activeIndex + 1);
            $('.Rent_slidesAmount-mobile .Rent_slideCurrent').text(this.activeIndex + 1);
        }
    }
});

$(window).on('resize', function () {
    RentSlider.update();
});

let planSlides = RentSliderWrp.find('.Rent_slide');
planSlides.each(function () {
    let planSelect = $(this).find('.Rent_planSelectItem');
    let planImg = $(this).find('.Rent_plans .Rent_planImg');
    planSelect.on('click', function () {
        planSelect.removeClass('Rent_planSelectItem-active');
        $(this).addClass('Rent_planSelectItem-active');
        planImg.removeClass('Rent_planImg-active');
        planImg.eq($(this).index()).addClass('Rent_planImg-active');
    })
});