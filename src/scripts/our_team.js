new Swiper('.OurTeam_slider', {
    effect: 'slide',
    slidesPerView: 4,
    speed: 600,
    loop: false,
    spaceBetween: 20,
    resistanceRatio: 0.5,
    navigation: {
        nextEl: '.OurTeam_slider_btn-next',
        prevEl: '.OurTeam_slider_btn-prev',
    },
    scrollbar: {
        el: '.OurTeam_scrollbar',
        hide: false,
        draggable: true,
        snapOnRelease: true,
        dragSize: '83',
    },
    pagination: {
        el: '.OurTeam_pagination',
        type: 'fraction',
    },
    breakpoints: {
        1283: {
            slidesPerView: 2.3,
        },
        767: {
            slidesPerView: 1.3,
            scrollbar: {
                dragSize: '45',
            },
        },
    }
});