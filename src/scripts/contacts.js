const $divContactsMap = $('.Contacts_map');

const contactsMap = new GMaps({
    div: '.Contacts_map',
    lat: $divContactsMap.data('lat'),
    lng: $divContactsMap.data('lng'),
    disableDefaultUI: true,
    zoomControl: true,
    zoom: 13,
    styles: APP_CONFIGS.gmapsStyles,
});

contactsMap.addMarker({
    lat: $divContactsMap.data('lat'),
    lng: $divContactsMap.data('lng'),
    title: 'Центральный офис',
    icon: {
        url: '/img/map_pin.png',
        scaledSize: new google.maps.Size(37, 52),
    },
});