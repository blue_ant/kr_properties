var mainSliderPrev = document.getElementById("mainSliderPrev");
var mainSliderNext = document.getElementById("mainSliderNext");

var mainSlider = new Swiper(".MainSlider_container", {
    effect: "fade",
    loop: true,
    allowTouchMove: false,
    pagination: {
        el: ".MainSlider_pagination",
        type: "bullets",
        clickable: true,
    },
    navigation: {
        nextEl: ".slider-button-next",
        prevEl: ".slider-button-prev",
    },
    on: {
        init: function() {
            var curSlide = this.realIndex + 1;
            var prevSlide = this.realIndex;
            var nextSlide = this.realIndex + 2;
            var allSlides = this.slides.length - 2;
            if (curSlide === 1) {
                mainSliderPrev.innerHTML = ("00" + allSlides).slice(-2);
            } else {
                mainSliderPrev.innerHTML = ("00" + prevSlide).slice(-2);
            }
            if (curSlide === allSlides) {
                mainSliderNext.innerHTML = "01";
            } else {
                mainSliderNext.innerHTML = ("00" + nextSlide).slice(-2);
            }
            $(".MainSlider_slide_numerator_current").each(function() {
                this.innerHTML = ("00" + curSlide).slice(-2);
            });
            $(".MainSlider_slide_numerator_next").each(function() {
                this.innerHTML = ("00" + allSlides).slice(-2);
            });
        },
        slideChange: function() {
            var curSlide = this.realIndex + 1;
            var prevSlide = this.realIndex;
            var nextSlide = this.realIndex + 2;
            var allSlides = this.slides.length - 2;
            if (curSlide === 1) {
                mainSliderPrev.innerHTML = ("00" + allSlides).slice(-2);
            } else {
                mainSliderPrev.innerHTML = ("00" + prevSlide).slice(-2);
            }
            if (curSlide === allSlides) {
                mainSliderNext.innerHTML = "01";
            } else {
                mainSliderNext.innerHTML = ("00" + nextSlide).slice(-2);
            }
            $(".MainSlider_slide_numerator_current").each(function() {
                this.innerHTML = ("00" + curSlide).slice(-2);
            });
            $(".MainSlider_slide_numerator_next").each(function() {
                this.innerHTML = ("00" + allSlides).slice(-2);
            });
        },
    },
});

$(".MainSlider_slide_infrastructure_toggle").on("click", () => {
    $(".MainSlider_slide_infrastructure").toggleClass("MainSlider_slide_infrastructure-open");
});

if ($(document).width() < 768) {
    $(".MainSlider_slide_infrastructure").removeClass("MainSlider_slide_infrastructure-open");
}

$(".MainContentBlock_slider").each(function() {
    var thisSlider = this;
    var mainContentBlockSlider = new Swiper($(this)[0], {
        effect: "fade",
        loop: true,
        allowTouchMove: false,
        navigation: {
            nextEl: ".MainContentBlock_slider_nav-next",
            prevEl: ".MainContentBlock_slider_nav-prev",
        },
        on: {
            init: function() {
                var curSlide = this.realIndex + 1;
                var allSlides = this.slides.length - 2;
                $(thisSlider)
                    .find(".MainContentBlock_slide_num_current")
                    .each(function() {
                        this.innerHTML = ("00" + curSlide).slice(-2);
                    });
                $(thisSlider)
                    .find(".MainContentBlock_slide_num_next")
                    .each(function() {
                        this.innerHTML = ("00" + allSlides).slice(-2);
                    });
            },
            slideChange: function() {
                var curSlide = this.realIndex + 1;
                var allSlides = this.slides.length - 2;
                $(thisSlider)
                    .find(".MainContentBlock_slide_num_current")
                    .each(function() {
                        this.innerHTML = ("00" + curSlide).slice(-2);
                    });
                $(thisSlider)
                    .find(".MainContentBlock_slide_num_next")
                    .each(function() {
                        this.innerHTML = ("00" + allSlides).slice(-2);
                    });
            },
        },
    });
});

let itemsMap = new GeoMap("#itemsHomepageMap");

$(".MainContentBlock_slide_infoTable_onMap").on("click", (event) => {
    let coords = $(event.currentTarget).data("latlng");
    let map = itemsMap.gmapsInst;
    $.fancybox.open({
        touch: false,
        src: "#popupMap1",
        beforeShow: () => {
            map.removeMarkers();
            map.addMarker({
                lat: coords[0],
                lng: coords[1],
                icon: {
                    url: "/img/map_pin.png",
                    scaledSize: new google.maps.Size(37, 52),
                },
            });

            map.setCenter({
                lat: coords[0],
                lng: coords[1],
            });
        },
    });
});
